/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef LGLAPPLICATION_H
#define LGLAPPLICATION_H

#include <QObject>
#include <Application.h>
class LGLApplication :public Application
{
    Q_OBJECT
public:
    explicit LGLApplication(QObject *parent = 0);

signals:

public slots:
};

#endif // LGLAPPLICATION_H
