/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#ifndef LGLOBJECFACTORY_H
#define LGLOBJECFACTORY_H

#include <ObjectFactory.h>
class LGLObjecFactory : public ObjectFactory
{
public:
    LGLObjecFactory();
    virtual MainWnd* CreateMainWnd();
  virtual EntityFactory* CreateEntityFactory();
};

#endif // LGLOBJECFACTORY_H
