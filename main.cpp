/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/


#include <QApplication>
#include <LGLObjecFactory.h>
#include <LGLApplication.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LGLObjecFactory oFactory;
    LGLApplication oApp;
    oApp.Initialize(&a, &oFactory);


    return a.exec();
}
