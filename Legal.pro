#-------------------------------------------------
#
# Project created by QtCreator 2016-06-04T11:37:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
INCLUDEPATH += "../ApplicationBase"
DEPENDPATH += "../ApplicationBase"
#RC_FILE = crm.rc
include(../ApplicationBase/ApplicationBase.pri)
TARGET = Legal
TEMPLATE = app


SOURCES += $$PWD/main.cpp\
    $$PWD/LGLObjecFactory.cpp \
    $$PWD/LGLEntityFactory.cpp \
    $$PWD/LGLMainWindow.cpp \
    $$PWD/LGLApplication.cpp

HEADERS  += \
    $$PWD/LGLObjecFactory.h \
    $$PWD/LGLEntityFactory.h \
    $$PWD/LGLMainWindow.h \
    $$PWD/LGLApplication.h

FORMS    += $$PWD/MainWnd.ui
