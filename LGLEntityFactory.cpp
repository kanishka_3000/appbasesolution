/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "LGLEntityFactory.h"

LGLEntityFactory::LGLEntityFactory()
{

}

void LGLEntityFactory::RegisterEntities()
{
    EntityFactory::RegisterEntities();
}

