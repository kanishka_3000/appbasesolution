/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by Kanishka Weerasekara                          *
 *                                                                         *
 *   kkgweerasekara@gmail.com                                              *
 *                                                                         *
 ***************************************************************************/

#include "LGLObjecFactory.h"
#include <LGLMainWindow.h>
#include <LGLEntityFactory.h>
LGLObjecFactory::LGLObjecFactory()
{

}

MainWnd *LGLObjecFactory::CreateMainWnd()
{
    return new LGLMainWindow();
}

EntityFactory *LGLObjecFactory::CreateEntityFactory()
{
    return new LGLEntityFactory();
}

